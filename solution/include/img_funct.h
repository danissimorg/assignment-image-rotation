#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H

public static Bitmap dilate_erode(
    this Bitmap sourceBitmap,  
    int matrixSize, 
    MorphologyType morphType, 
    bool newB = true, 
    bool newG = true, 
    bool newR = true );

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_MANIP_H
