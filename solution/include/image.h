#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <stdio.h>
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
